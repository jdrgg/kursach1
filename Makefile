SOURCES=\
    main.cs \
    logic/Account.cs \
    logic/Budget.cs \
    logic/Currency.cs \
    logic/Expense.cs \
    logic/Income.cs \
    logic/Operation.cs \
    logic/Transaction.cs \
    ui/CLI.cs


all: clean run

main.exe: $(SOURCES)
	csc $(SOURCES)

run: main.exe
	mono main.exe

clean:
	rm -f main.exe

watch: all
	find . -name "*.cs" | entr -prcs "make"