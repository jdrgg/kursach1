
using System;
using Logic;

namespace UI {
    class GUI {
        bool isRunning = true;
        Budget budget;

        public GUI() {
            this.budget = new Budget();
        }

        public void mainLoop() {
            clearScreen();
            while(isRunning) {
                mainMenu();
            }
        }


        private void mainMenu() {
            switch(menu("Select action", new string[] {
                "List accounts",
                "Add account",
                "Select account",
                "Exit"
            })) {
                case "List accounts":
                    listAccounts();
                    return;
                case "Add account":
                    addAccount();
                    return;
                case "Select account":
                    accountMenu();
                    return;
                case "Exit":
                    isRunning = false;
                    return;
                
            }
        }
        private void listAccounts() {
            var accounts = this.budget.listAccounts();

            if(accounts.Count == 0) {
                Console.WriteLine("No accounts created yet");
                Console.WriteLine("");
                return;
            }
            Console.WriteLine($"Accounts({accounts.Count}):");
            foreach(var acc in accounts) {
                Console.WriteLine($"  {acc}");
            }
            Console.WriteLine("");
        }

        private void addAccount() {
            string name = promt("Enter Name");

            try {
                this.budget.addAccount(name);
                Console.WriteLine($"Account \"{name}\" created.\n");
            } catch(Exception e) {
                Console.WriteLine($"Error: {e.Message}\n");
            }
        }



        private void accountMenu() {
            string name = promt("Enter name");
            if(!this.budget.hasAccount(name)) {
                Console.WriteLine($"Account \"{name}\" not exist.\n");
                return;
            }   
            while(true) {
                switch(menu($"Account \"{name}\", balance: {this.budget.getBalance(name)}", new string[] {
                    "Add income",
                    "Add expense",
                    "Add transaction",
                    "View all income",
                    "View income by type",
                    "View all expense",
                    "View expense by type",
                    "Back"
                })) {
                    case "Add income":
                        addIncome(name);
                        continue;
                    case "Add expense":
                        addExpense(name);
                        continue;
                    case "Add transaction":
                        addTransaction(name);
                        continue;
                    case "View all income":
                        viewAllIncome(name);
                        continue;
                    case "View income by type":
                        viewIncomeByType(name);
                        continue;
                    case "View all expense":
                        viewAllExpense(name);
                        continue;
                    case "View expense by type":
                        viewExpenseByType(name);
                        continue;
                        
                    case "Back":
                        return;
                }
                clearScreen();
            }
        }

        private void viewAllIncome(string name) {
            Console.WriteLine($"Income: {this.budget.getIncome(name)}");
        }
        private void viewIncomeByType(string name) {
            var stats = this.budget.getIncomeByType(name);
            Console.WriteLine($"Types({stats.Count}):");
            foreach(var stat in stats) {
                Console.WriteLine($"  \"{stat.Key}\": {stat.Value}");
            }
            Console.WriteLine("");
        }
        private void viewAllExpense(string name) {
            Console.WriteLine($"Expense: {this.budget.getExpense(name)}");
        }
        private void viewExpenseByType(string name) {
            var stats = this.budget.getExpenseByType(name);
            Console.WriteLine($"Types({stats.Count}):");
            foreach(var stat in stats) {
                Console.WriteLine($"  \"{stat.Key}\": {stat.Value}");
            }
            Console.WriteLine("");
        }
        private void addIncome(string name) {
            string type = promt("Enter type");
            double amount = -1.0;

            try {
                amount = Double.Parse(promt("Enter amount"));
            }
            catch (FormatException) {
                clearScreen();
                Console.WriteLine("Amount should be a number.");
                return;
            }
            this.budget.applyIncome(name, type, new Currency((long)(amount*100)));
        }
        private void addExpense(string name) {
            string type = promt("Enter type");
            double amount = -1.0;

            try {
                amount = Double.Parse(promt("Enter amount"));
            }
            catch (FormatException) {
                clearScreen();
                Console.WriteLine("Amount should be a number.");
                return;
            }
            try {
                this.budget.applyExpense(name, type, new Currency((long)(amount*100)));
            } catch(Exception e) {
                Console.WriteLine($"Error: {e.Message}\n");
            }
        }
        private void addTransaction(string name) {
            string to = promt("Enter receiver");
            if(!this.budget.hasAccount(to)) {
                Console.WriteLine($"Account \"{name}\" not found");
                return;
            }

            double amount = -1.0;

            try {
                amount = Double.Parse(promt("Enter amount"));
            }
            catch (FormatException) {
                clearScreen();
                Console.WriteLine("Amount should be a number.");
                return;
            }
            this.budget.applyTransaction(name, to, new Currency((long)(amount*100)));
        }

        private string menu(string title, string[] options) {
            while(true) {
                Console.WriteLine(title);
                for(int i = 0;i < options.Length;i++) {
                    Console.WriteLine("  " + (i+1) + ". " + options[i]);            
                }
                int index = -1;
                try {
                    index = Int32.Parse(promt("Choose"));
                }
                catch (FormatException) {
                    clearScreen();
                    Console.WriteLine("Answer should be a number.");
                    continue;
                }

                if(index < 1 || index > options.Length) {
                    clearScreen();
                    Console.WriteLine($"Option {index} doesn't exist");
                    continue;
                }

                clearScreen();


                return options[index - 1];
            }
        }

        private string promt(string question) {
            Console.Write(question+": ");
            return Console.ReadLine();
        }

        private void clearScreen() {
                Console.Write("\x1b[2J");
        }
    }
}