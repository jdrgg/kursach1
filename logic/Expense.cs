namespace Logic {

    class Expense : Operation {
        string title;
        Currency value;

        public Expense(string title, Currency value) {
            this.title = title;
            this.value = value;
        }

        override public string getTitle() {
            return this.title;
        }
        override public Currency amount(Account account) {
            return this.value.invert();
        }
    }
}