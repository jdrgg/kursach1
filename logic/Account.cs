
using System;
using System.Collections.Generic;


namespace Logic {
    class Account {
        public string name;
        public Currency balance;
        List<Operation> history;

        public Account(string name) {
            this.name = name;
            this.balance = Currency.zero();
            this.history = new List<Operation>();
        }

        public void apply(Operation op) {
            var amount = op.amount(this);

            if(this.balance.add(amount).less(Currency.zero())) {
                throw new Exception("Account "+this.name+": has no overdraft");
            }
            this.history.Add(op);
            this.balance.addTo(amount);
        }

        public Currency getIncome() {
            var sum = Currency.zero();

            foreach(var op in this.history) {
                var amount = op.amount(this);
                if(amount.less(Currency.zero())) {
                    continue;
                }
                sum.addTo(amount);
            }

            return sum;
        }
        public Dictionary<string, Currency> getIncomeByType() {
            var stats = new Dictionary<string, Currency>();
            
            foreach(var op in this.history) {
                var amount = op.amount(this);
                if(amount.less(Currency.zero())) {
                    continue;
                }
                if(!stats.ContainsKey(op.getTitle())) {
                    stats.Add(op.getTitle(), Currency.zero());
                }
                stats[op.getTitle()].addTo(amount);
            }

            return stats;
        }
        public Currency getExpense() {
            var sum = Currency.zero();

            foreach(var op in this.history) {
                var amount = op.amount(this);
                if(!amount.less(Currency.zero())) {
                    continue;
                }
                sum.addTo(amount);
            }

            return sum.invert();
        }
        public Dictionary<string, Currency> getExpenseByType() {
            var stats = new Dictionary<string, Currency>();
            
            foreach(var op in this.history) {
                var amount = op.amount(this);
                if(!amount.less(Currency.zero())) {
                    continue;
                }
                if(!stats.ContainsKey(op.getTitle())) {
                    stats.Add(op.getTitle(), Currency.zero());
                }
                stats[op.getTitle()].addTo(amount.invert());
            }

            return stats;
        }
        override public string ToString() {
            return $"Account(\"{name}\", {this.balance}, {this.history.Count})";
        }

    }
}