
using System;

namespace Logic {
    
    class Transaction : Operation {
        string sender;
        string receiver;
        Currency value;

        public Transaction(string sender, string receiver, Currency value) {
            this.sender = sender;
            this.receiver = receiver;
            this.value = value;

            if(value.less(Currency.zero())) {
                throw new Exception("Only forward transactions allowed");
            }
        }

        override public string getTitle() {
            return $"Transaction from {this.sender} to {this.receiver}";
        }
        override public Currency amount(Account account) {
            if(account.name == this.sender) {
                return this.value.invert();
            }
            if(account.name == this.receiver) {
                return this.value;
            }
            throw new Exception("Unreacheble");
        }
    }
}