namespace Logic {
    class Currency {
        long value;

        public Currency(long value) {
            this.value = value;
        }
        
        public void addTo(Currency c) {
            this.value += c.value;
        }
        public Currency add(Currency c) {
            return new Currency(this.value + c.value);
        }
        public bool less(Currency c) {
            return this.value < c.value;
        }
        public Currency invert() {
            return new Currency(-this.value);
        }
        override public string ToString() {
            return $"{this.value/100.0}";
        }

        public static Currency zero() {
            return new Currency(0);
        }


    }
}