

using System;
using System.Collections.Generic;

namespace Logic {
    class Budget {
        Dictionary<string, Account> accounts;

        public Budget() {
            this.accounts = new Dictionary<string, Account>();
        }

        private Account findAccountOrFail(string name) {
            if(!this.accounts.ContainsKey(name)) {
                throw new Exception("Account with name "+name+" not found");
            }
            return this.accounts[name];
        }

        public void addAccount(string name) {
            if(this.accounts.ContainsKey(name)) {
                throw new Exception("Account with name "+name+" already exist");
            }
            this.accounts.Add(name, new Account(name));
        }
        public bool hasAccount(string name) {
            return this.accounts.ContainsKey(name);
        }
        public List<string> listAccounts() {
            return new List<string>(this.accounts.Keys);
        }
        public Currency getBalance(string name) {
            return this.findAccountOrFail(name).balance;
        }
        public void applyIncome(string name, string type, Currency amount) {
            this.findAccountOrFail(name).apply(new Income(type, amount));
        }
        public void applyExpense(string name, string type, Currency amount) {
            this.findAccountOrFail(name).apply(new Expense(type, amount));
        }
        public void applyTransaction(string from, string to, Currency amount) {
            Account fromAcc = this.findAccountOrFail(from);
            Account toAcc = this.findAccountOrFail(to);

            Transaction transactions = new Transaction(from, to , amount);

            //First apply to the sender because sender may not have anought currency
            fromAcc.apply(transactions);
            toAcc.apply(transactions);
        }

        public Currency getIncome(string name) {
            return this.findAccountOrFail(name).getIncome();
        }
        public Dictionary<string, Currency> getIncomeByType(string name) {
            return this.findAccountOrFail(name).getIncomeByType();
        }
        public Currency getExpense(string name) {
            return this.findAccountOrFail(name).getExpense();
        }
        public Dictionary<string, Currency> getExpenseByType(string name) {
            return this.findAccountOrFail(name).getExpenseByType();
        }

        override public string ToString() {
            var accs = "";
            foreach(var pair in this.accounts) {
                accs += "  "+pair.Value.ToString()+"\n";
            }
            return $"Budget(\n{accs})";
        }

    }
}