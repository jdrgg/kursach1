namespace Logic {
    abstract class Operation {
        public abstract string getTitle();
        public abstract Currency amount(Account account);
    }
}