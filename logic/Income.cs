namespace Logic {
    class Income : Operation {
        string title;
        Currency value;

        public Income(string title, Currency value) {
            this.title = title;
            this.value = value;
        }

        override public string getTitle() {
            return this.title;
        }
        override public Currency amount(Account account) {
            return this.value;
        }
    }
}